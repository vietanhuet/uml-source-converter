package DrawingGUI;

import java.io.Serializable;
// Dimension
public class Dimension implements Serializable {
	private static final long serialVersionUID = 1L;
	public int width, height;
	public Dimension(int width, int height) {
		this.height = height;
		this.width = width;
	}
}