package DrawingGUI;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class MyDialogPane extends JPanel {
	private static final long serialVersionUID = 1L;
	public MyDialogPane() {
		super();
	}
	public void paint(Graphics g) {
		ImageIcon icon = new ImageIcon("src/Images/About.bin");
		g.drawImage(icon.getImage(), 5, 5, 391, 169, null);
	}
}
